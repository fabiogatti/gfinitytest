# gfinitytest

## My thought process

First of all I wanna thank you for taking the time to check out my test.
So after I saw the figma files describing the pages i had a general idea on how to proceed and roughly the components that the page would be having.
I started obviously with the list page, building the navbar component and then proceeded to add test the functionality for the sanity client, which thanfully didnt take me too long. After looking at the data I started to code the ***TitleSort*** component (Knowing that I would have to implement the sorting functionality later) and proceeded to code both the ***StatBox*** and the ***ListItem*** component. After that was done, I tested how it would look on the mobile version and made the corresponding changes.
After the first page was finished (adding some minor details to make it look closer to the design) I proceeded with the next page, which looked a bit simpler that the previous one. I started to load the image and, after a bit of troubleshooting, I finally made it. After that it was just a matter of creating the ***StatList*** and adjusting it with the data to make the layout as close as the designs in the figma file.

Hope you enjoy exploring my project, I know that it could've been a little more clean but for the sake of not overextending the deadline I think this is a good compromise.

Please be sure to let me know if you have any questions, thanks again and have a great day!

* *P.S: I'm leaving the build section just in case* *

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).
