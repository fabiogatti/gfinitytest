/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    colors: {
      black:"#000000",
      lightBlack:"#161616",
      listBlack:"#101010",
      white:'#ffffff',
      grey:"#606060",
      lightGrey:"#2C2C31",
      cardGrey:"#848282"
    },
    extend: {},
  },
  plugins: [],
}

